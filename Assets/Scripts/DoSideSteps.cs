﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class DoSideSteps : MonoBehaviour
{
    private Vector3 start;
    private Vector3 end;
    private float progress;
    private DateTime startTime;
    private int stepLength;
    private float speed;
    public bool right;


    void Start()
    {
        if (right == true)
        {
            transform.Rotate(0.0f, 90.0f, 0.0f, Space.Self);
        }
        else
        {
            transform.Rotate(0.0f, -90.0f, 0.0f, Space.Self);
        }
        start = transform.position;
        end = start;
        stepLength = 40;
        speed = 0.0005f - UnityEngine.Random.Range(150, 400) * 0.000001f;
    }

    void FixedUpdate()
    {
        if (right == true)
        {
            tryToStepRight();
        }
        else if (right == false)
        {
            tryToStepLeft();
        }
        progress = (long)(DateTime.UtcNow - startTime).TotalMilliseconds * speed;
        move();
    }

    private bool isAvailable()
    {
        bool answer;
        if ((long)(DateTime.UtcNow - startTime).TotalMilliseconds < (1 / speed + 50))
        {
            answer = false;
        }
        else
        {
            answer = true;
        }
        return answer;
    }

    private void tryToStepRight()
    {
        if (transform.position[0] >= start[0] + stepLength)
        {
            transform.position = start;
        }
        if (isAvailable())
        {
            start = transform.position;
            startTime = DateTime.UtcNow;
            end = new Vector3(start[0] + stepLength, start[1], start[2]);
        }
    }

    private void tryToStepLeft()
    {
        if (transform.position[0] <= start[0] + stepLength)
        {
            transform.position = start;
        }
        if (isAvailable())
        {
            start = transform.position;
            startTime = DateTime.UtcNow;
            end = new Vector3(start[0] - stepLength, start[1], start[2]);
        }

    }


    private void move()
    {
        transform.position = Vector3.Lerp(start, end, progress);
    }

}
