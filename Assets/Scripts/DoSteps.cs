﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DoSteps : MonoBehaviour
{
    public Vector3 start;
    public Vector3 end;
    private float progress;
    private int stepLength;
    public float stepTime;
    private int intLevelProgress;
    private int record;
    private Rigidbody rb;

    public Text levelProgress;


    void Start()
    {
        start = transform.position;
        end = start;
        stepLength = 1;
        stepTime = 0.2f;
        intLevelProgress = 0;
        record = PlayerPrefs.GetInt("record");
        Application.targetFrameRate = 60;
        rb = GetComponent<Rigidbody>();
    }

    public void setIntLevelProgress(int progress)
    {
        intLevelProgress = progress;
    }

    void FixedUpdate()
    {
        if (isAvailable()) goBack();
        levelProgress.text = "Progress = " + intLevelProgress;
    }

    public float getLevelProgress()
    {
        return (int)transform.position[2];
    }


    public int getRecord()
    {
        return record;
    }

    private bool isAvailable()
    {
        bool answer;

        if (transform.position[0] % 1 == 0 & transform.position[2] % 1 == 0)
        {
            answer = true;
        }
        else answer = false;
        return answer;
    }

    public void tryToStepRight()
    {
        if (isAvailable())
        {
            start = transform.position;
            end = new Vector3(start[0] + stepLength, start[1], start[2]);
            transform.DOJump(end, 1, 1, stepTime);
        }
    }

    public void tryToStepLeft()
    {
        if (isAvailable())
        {
            start = transform.position;
            end = new Vector3(start[0] - stepLength, start[1], start[2]);
            transform.DOJump(end, 1, 1, stepTime);
        }
    }

    public void tryToStepForward()
    {
        if(isAvailable())
        {
            start = transform.position;
            if (transform.position[2] + 2 > intLevelProgress) intLevelProgress = (int)transform.position[2] + 2;
            if (intLevelProgress > record) record = intLevelProgress;
            end = new Vector3(start[0], start[1], start[2] + stepLength);
            transform.DOJump(end, 0.5f, 1, stepTime);
        }
    }

    public void tryToStepBackward()
    {
        if (isAvailable())
        {
            if (intLevelProgress - transform.position[2] < 5)
            {
                start = transform.position;
                end = new Vector3(start[0], start[1], start[2] - stepLength);
                transform.DOJump(end, 1, 1, stepTime);
            }
        }
    }

    public void goBack()
    {
        transform.position = end;
    }

}
