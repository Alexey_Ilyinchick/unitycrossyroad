﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObjects : MonoBehaviour
{
    public GameObject blocks;
    public GameObject cars;
    public GameObject coin;


    void Start()
    {

        Vector3 position = transform.position;
        for (float x = 10f; x < 40; x++)
        {
            for (float z = position[2]; z < position[2] + 50; z += 2)
            {
                if (x == 10f)
                {
                    if (Random.Range(0, 10) < 5)
                    {
                        cars.gameObject.GetComponent<DoSideSteps>().right = true;
                        GameObject car = Instantiate(cars, new Vector3(5f, 0f, z + 1), Quaternion.identity) as GameObject;
                        car.transform.SetParent(this.transform);
                    }
                    else
                    {
                        cars.gameObject.GetComponent<DoSideSteps>().right = false;
                        GameObject car = Instantiate(cars, new Vector3(45f, 0f, z + 1), Quaternion.identity) as GameObject;
                        car.transform.SetParent(this.transform);
                    }
                }

                if (Random.Range(0, 10) < 2)
                {
                    GameObject bush = Instantiate(blocks, new Vector3(x, 0f, z), Quaternion.identity) as GameObject;
                    bush.transform.SetParent(this.transform);
                }
                else if (Random.Range(0, 100) < 1)
                {
                    GameObject coinObject = Instantiate(coin, new Vector3(x, 0.5f, z), new Quaternion(0,90,90,1)) as GameObject;
                    coinObject.transform.SetParent(this.transform);
                }
                if (Random.Range(0, 100) < 1)
                {
                    GameObject coinObject = Instantiate(coin, new Vector3(x, 0.5f, z+1), new Quaternion(0, 90, 90, 1)) as GameObject;
                    coinObject.transform.SetParent(this.transform);
                }
            }
        }
    }
}
