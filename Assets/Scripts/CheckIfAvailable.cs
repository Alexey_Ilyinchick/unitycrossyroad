﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckIfAvailable : MonoBehaviour
{
    private Rigidbody rb;
    public Transform player;
    public GameObject chunk;
    public Text coinsText;
    public Canvas canvas;

    private int coinsCounter;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player = transform.parent;
        coinsCounter = PlayerPrefs.GetInt("totalCoins");
        coinsText.text = "Coins = " + coinsCounter;
    }


    void playHit()
    {
        GetComponent<AudioSource>().Play(0);
    }

    void FixedUpdate()
    {
        transform.position = transform.parent.GetComponent<DoSteps>().end;
        checkOnOutOfBounds();
    }

    void checkOnOutOfBounds()
    {
        if (transform.position[0] > 39 || transform.position[0] < 10)
        {
            transform.parent.GetComponent<DoSteps>().end = transform.parent.GetComponent<DoSteps>().start;
        }
        if (transform.position[2] < -4)
        {
            transform.parent.GetComponent<DoSteps>().end = transform.parent.GetComponent<DoSteps>().start;
        }
    }


    void returnToPlayer()
    {
        transform.position = transform.parent.GetComponent<DoSteps>().start;
    }


    private void die()
    {
        playHit();
        PlayerPrefs.SetInt("totalCoins", coinsCounter);
        PlayerPrefs.SetInt("record", transform.parent.GetComponent<DoSteps>().getRecord());
        canvas.GetComponent<UIthings>().setData(coinsCounter, transform.parent.GetComponent<DoSteps>().getLevelProgress()+2, transform.parent.GetComponent<DoSteps>().getRecord());
        canvas.GetComponent<UIthings>().showDeathMenu();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            die();
        }
        else if (other.tag == "Coin")
        {
            other.gameObject.SetActive(false);
            coinsCounter++;
        }
        else
        {
            transform.parent.GetComponent<DoSteps>().end = transform.parent.GetComponent<DoSteps>().start;
        }
        returnToPlayer();
    }
}
