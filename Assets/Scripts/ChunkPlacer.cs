﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkPlacer : MonoBehaviour
{
    public Transform player;
    public Chunk[] chunkPrefabs;
    public Chunk firstChunk;

    private List<Chunk> spawnedChunks = new List<Chunk>();

    // Start is called before the first frame update
    void Start()
    {
        spawnedChunks.Add(firstChunk);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (player.position.z > spawnedChunks[spawnedChunks.Count - 1].End.position.z - 15)
        {
            spawnChunk();
        }
    }

    void spawnChunk()
    {
        
        Chunk newChunk = Instantiate(chunkPrefabs[0]);
        newChunk.transform.position = spawnedChunks[spawnedChunks.Count - 1].End.position - newChunk.Begin.localPosition;
        spawnedChunks.Add(newChunk);
        Debug.Log(spawnedChunks.Count);
        if (spawnedChunks.Count>3)
        {
            foreach(Transform child in spawnedChunks[0].transform)
            {
                GameObject.Destroy(child.gameObject);
            }
            Destroy(spawnedChunks[1].gameObject);
            spawnedChunks.RemoveAt(1);
        }
    }
}
