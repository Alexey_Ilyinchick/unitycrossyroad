﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIthings : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject playMenu;
    public GameObject deathMenu;
    public GameObject listener;
    public GameObject player;

    public Text coinsText;
    public Text levelProgressText;
    public Text recordText;
    public Text coinsCountText;

    private int coinsCount;
    private int record;
    private float levelProgress;

    void Start()
    {
        mainMenu.SetActive(true);
        playMenu.SetActive(false);
        deathMenu.SetActive(false);
        if (PlayerPrefs.GetInt("sound") == 1)
        {
            listener.SetActive(true);
        } else if (PlayerPrefs.GetInt("sound") == 0)
        {
            listener.SetActive(false);
        }
    }

    private void hideStartMenu()
    {
        mainMenu.SetActive(false);
        playMenu.SetActive(true);
        deathMenu.SetActive(false);
    }

    public void showStartMenu()
    {
        mainMenu.SetActive(true);
        playMenu.SetActive(false);
        deathMenu.SetActive(false);
    }

    public void reloadClick()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


    public void playMenuClick()
    {
        hideStartMenu();
        player.GetComponent<DoSteps>().end = new Vector3(25, 0f, -2f);
        player.GetComponent<DoSteps>().setIntLevelProgress(0);

    }

    public void setData(int coinsCount, float levelProgress, int record) 
    {
        this.levelProgress = levelProgress;
        this.coinsCount = coinsCount;
        this.record = record;
    }

    public void muteSound()
    {
        listener.SetActive(!listener.activeSelf);
        if (listener.activeSelf)
        {
            PlayerPrefs.SetInt("sound", 1);
        } else if (!listener.activeSelf)
        {
            PlayerPrefs.SetInt("sound", 0);
        }
    }

    public void showDeathMenu()
    {
        mainMenu.SetActive(false);
        playMenu.SetActive(false);
        deathMenu.SetActive(true);
        coinsCountText.text = "Coins = " + coinsCount;
        levelProgressText.text = "Progress = " + levelProgress;
        recordText.text = "Record = " + record; 
    }


}